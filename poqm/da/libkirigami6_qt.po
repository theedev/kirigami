# Martin Schlander <mschlander@opensuse.org>, 2017, 2018, 2019, 2020.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"PO-Revision-Date: 2020-10-27 19:16+0100\n"
"Last-Translator: Martin Schlander <mschlander@opensuse.org>\n"
"Language-Team: Danish <kde-i18n-doc@kde.org>\n"
"Language: da\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Qt-Contexts: true\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 20.04.2\n"

#: controls/AboutItem.qml:139
#, qt-format
msgctxt "AboutItem|"
msgid "%1 (%2)"
msgstr ""

#: controls/AboutItem.qml:148
#, fuzzy, qt-format
#| msgctxt "AboutPage|"
#| msgid "Send an email to %1"
msgctxt "AboutItem|"
msgid "Send an email to %1"
msgstr "Send en e-mail til %1"

#: controls/AboutItem.qml:198
msgctxt "AboutItem|"
msgid "Get Involved"
msgstr ""

#: controls/AboutItem.qml:204
msgctxt "AboutItem|"
msgid "Donate"
msgstr ""

#: controls/AboutItem.qml:221
msgctxt "AboutItem|"
msgid "Report a Bug"
msgstr ""

#: controls/AboutItem.qml:234
#, fuzzy
#| msgctxt "AboutPage|"
#| msgid "Copyright"
msgctxt "AboutItem|"
msgid "Copyright"
msgstr "Ophavsret"

#: controls/AboutItem.qml:278
#, fuzzy
#| msgctxt "AboutPage|"
#| msgid "License:"
msgctxt "AboutItem|"
msgid "License:"
msgstr "Licens:"

#: controls/AboutItem.qml:300
#, fuzzy, qt-format
#| msgctxt "AboutPage|"
#| msgid "License: %1"
msgctxt "AboutItem|"
msgid "License: %1"
msgstr "Licens: %1"

#: controls/AboutItem.qml:311
#, fuzzy
#| msgctxt "AboutPage|"
#| msgid "Libraries in use"
msgctxt "AboutItem|"
msgid "Libraries in use"
msgstr "Biblioteker i brug"

#: controls/AboutItem.qml:341
#, fuzzy
#| msgctxt "AboutPage|"
#| msgid "Authors"
msgctxt "AboutItem|"
msgid "Authors"
msgstr "Ophavsmænd"

#: controls/AboutItem.qml:350
msgctxt "AboutItem|"
msgid "Show author photos"
msgstr ""

#: controls/AboutItem.qml:381
#, fuzzy
#| msgctxt "AboutPage|"
#| msgid "Credits"
msgctxt "AboutItem|"
msgid "Credits"
msgstr "Tak"

#: controls/AboutItem.qml:394
#, fuzzy
#| msgctxt "AboutPage|"
#| msgid "Translators"
msgctxt "AboutItem|"
msgid "Translators"
msgstr "Oversættere"

#: controls/AboutPage.qml:93
#, fuzzy, qt-format
#| msgctxt "AboutPage|"
#| msgid "About"
msgctxt "AboutPage|"
msgid "About %1"
msgstr "Om"

#: controls/AbstractApplicationWindow.qml:204
msgctxt "AbstractApplicationWindow|"
msgid "Quit"
msgstr ""

#: controls/ActionToolBar.qml:205
msgctxt "ActionToolBar|"
msgid "More Actions"
msgstr "Flere handlinger"

#: controls/Avatar.qml:182
#, qt-format
msgctxt "Avatar|"
msgid "%1 — %2"
msgstr ""

#: controls/Chip.qml:82
msgctxt "Chip|"
msgid "Remove Tag"
msgstr ""

#: controls/ContextDrawer.qml:66
msgctxt "ContextDrawer|"
msgid "Actions"
msgstr "Handlinger"

#: controls/GlobalDrawer.qml:506
msgctxt "GlobalDrawer|"
msgid "Back"
msgstr "Tilbage"

#: controls/GlobalDrawer.qml:599
msgctxt "GlobalDrawer|"
msgid "Close Sidebar"
msgstr "Luk sidepanel"

#: controls/GlobalDrawer.qml:602
#, fuzzy
#| msgctxt "GlobalDrawer|"
#| msgid "Close Sidebar"
msgctxt "GlobalDrawer|"
msgid "Open Sidebar"
msgstr "Luk sidepanel"

#: controls/LoadingPlaceholder.qml:54
msgctxt "LoadingPlaceholder|"
msgid "Loading…"
msgstr ""

#: controls/PasswordField.qml:42
msgctxt "PasswordField|"
msgid "Password"
msgstr "Adgangskode"

#: controls/private/globaltoolbar/PageRowGlobalToolBarUI.qml:70
#, fuzzy
#| msgctxt "GlobalDrawer|"
#| msgid "Close Sidebar"
msgctxt "PageRowGlobalToolBarUI|"
msgid "Close menu"
msgstr "Luk sidepanel"

#: controls/private/globaltoolbar/PageRowGlobalToolBarUI.qml:70
msgctxt "PageRowGlobalToolBarUI|"
msgid "Open menu"
msgstr ""

#: controls/SearchField.qml:94
#, fuzzy
#| msgctxt "SearchField|"
#| msgid "Search"
msgctxt "SearchField|"
msgid "Search…"
msgstr "Søg"

#: controls/SearchField.qml:96
msgctxt "SearchField|"
msgid "Search"
msgstr "Søg"

#: controls/SearchField.qml:106
msgctxt "SearchField|"
msgid "Clear search"
msgstr ""

#: controls/settingscomponents/CategorizedSettings.qml:40
#: controls/settingscomponents/CategorizedSettings.qml:104
msgctxt "CategorizedSettings|"
msgid "Settings"
msgstr ""

#: controls/settingscomponents/CategorizedSettings.qml:40
#, qt-format
msgctxt "CategorizedSettings|"
msgid "Settings — %1"
msgstr ""

#. Accessibility text for a page tab. Keep the text as concise as possible and don't use a percent sign.
#: controls/swipenavigator/templates/PageTab.qml:38
#, qt-format
msgctxt "PageTab|"
msgid "Current page. Progress: %1 percent."
msgstr ""

#. Accessibility text for a page tab. Keep the text as concise as possible.
#: controls/swipenavigator/templates/PageTab.qml:41
#, qt-format
msgctxt "PageTab|"
msgid "Navigate to %1. Progress: %2 percent."
msgstr ""

#. Accessibility text for a page tab. Keep the text as concise as possible.
#: controls/swipenavigator/templates/PageTab.qml:46
msgctxt "PageTab|"
msgid "Current page."
msgstr ""

#. Accessibility text for a page tab that's requesting the user's attention. Keep the text as concise as possible.
#: controls/swipenavigator/templates/PageTab.qml:49
#, qt-format
msgctxt "PageTab|"
msgid "Navigate to %1. Demanding attention."
msgstr ""

#. Accessibility text for a page tab that's requesting the user's attention. Keep the text as concise as possible.
#: controls/swipenavigator/templates/PageTab.qml:52
#, fuzzy, qt-format
#| msgctxt "ForwardButton|"
#| msgid "Navigate Forward"
msgctxt "PageTab|"
msgid "Navigate to %1."
msgstr "Navigér fremad"

#: controls/templates/OverlayDrawer.qml:130
#, fuzzy
#| msgctxt "GlobalDrawer|"
#| msgid "Close Sidebar"
msgctxt "OverlayDrawer|"
msgid "Close drawer"
msgstr "Luk sidepanel"

#: controls/templates/OverlayDrawer.qml:136
msgctxt "OverlayDrawer|"
msgid "Open drawer"
msgstr ""

#: controls/templates/private/BackButton.qml:34
msgctxt "BackButton|"
msgid "Navigate Back"
msgstr "Navigér tilbage"

#: controls/templates/private/ForwardButton.qml:32
msgctxt "ForwardButton|"
msgid "Navigate Forward"
msgstr "Navigér fremad"

#: controls/ToolBarApplicationHeader.qml:89
msgctxt "ToolBarApplicationHeader|"
msgid "More Actions"
msgstr "Flere handlinger"

#: controls/UrlButton.qml:51
msgctxt "UrlButton|"
msgid "Copy Link to Clipboard"
msgstr ""

#: settings.cpp:211
#, qt-format
msgctxt "Settings|"
msgid "KDE Frameworks %1"
msgstr "KDE Frameworks %1"

#: settings.cpp:213
#, qt-format
msgctxt "Settings|"
msgid "The %1 windowing system"
msgstr "Vinduessystemet %1"

#: settings.cpp:214
#, qt-format
msgctxt "Settings|"
msgid "Qt %2 (built against %3)"
msgstr "Qt %2 (bygget op imod %3)"

#~ msgctxt "UrlButton|"
#~ msgid "Copy link address"
#~ msgstr "Kopiér linkadresse"

#~ msgctxt "SearchField|"
#~ msgid "Search..."
#~ msgstr "Søg..."

#~ msgctxt "AboutPage|"
#~ msgid "%1 <%2>"
#~ msgstr "%1 <%2>"

#~ msgctxt "ToolBarPageHeader|"
#~ msgid "More Actions"
#~ msgstr "Flere handlinger"
